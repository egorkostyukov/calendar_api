# calendar_api
---

Календарь с возможностями:

- отображения событий по дням/неделям/месяцам; 
- переход на предыдущий/следующий месяц; 
- создание новых событий;

## Как запустить проект в Linux?

- git clone https://bitbucket.org/egorkostyukov/calendar_api.git
- cd calendar_api

Если нужно установить виртуальную среду, то выполянем следующее:

- virtualenv env
- source env/bin/activate
- pip3 install django

Делаем миграции в базе и запускаем сервер:

- python3 manage.py migrate
- python3 manage.py runserver

Заходим на [http://127.0.0.1:8000/calendar/](http://127.0.0.1:8000/calendar/) и пользуемся календарём

## Как запустить проект в PyCharm?

- git clone https://bitbucket.org/egorkostyukov/calendar_api.git
- В PyCharm открываем проект **calendar_api**
- Запускаем **manage.py** (***Tools -> Run manage.py Task...*** или ***Ctrl + Alt + R***)

Выполняем миграции в проекте и в базе, и запускаем сервер:

- makemigrations calendar_app
- migrate
- runserver

Заходим на [http://127.0.0.1:8000/calendar/](http://127.0.0.1:8000/calendar/) и пользуемся календарём